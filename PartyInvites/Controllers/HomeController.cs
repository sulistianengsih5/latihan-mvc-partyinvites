﻿using Microsoft.AspNetCore.Mvc;
using PartyInvites.Models;
using System.Diagnostics;

namespace PartyInvites.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            int hour = DateTime.Now.Hour;
            ViewBag.Greeting = hour < 12 ? "Good Morning" : "Good Afternoon";
            // Menampilkan View dengan nama Index
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        // Menyatakan bahwa method dibawahnya khusus untuk HTTP GET Method
        [HttpGet]
        public IActionResult RsvpForm()
        {
            return View();
        }

        // Menyatakan bahwa method dibawahnya khusus untuk HTTP POST Method
        [HttpPost]
        public IActionResult RsvpForm(GuestResponse guestResponse)
        {
            if (ModelState.IsValid)
            {
                // Call a view called Thanks and pass the GuestResponse object
                return View("Thanks", guestResponse);
            } else
            {
                // there is a validation error
                return View();
            }
        }

    }
}